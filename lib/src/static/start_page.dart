import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';

class Startpage extends StatefulWidget {
  const Startpage({Key key}) : super(key: key);

  @override
  _StartpageState createState() => _StartpageState();
}

class _StartpageState extends State<Startpage> {
  bool isLoggedIn = false;
  @override
  Widget build(BuildContext context) {
    //final size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Color.fromRGBO(3, 128, 169, 1.0),
      resizeToAvoidBottomPadding: false,
      body: Column(
        children: <Widget>[
          Image.asset('assets/appfondo.JPG'),
          SizedBox(height: 100.0, width: 500.0),
          RaisedButton(
            padding: EdgeInsets.symmetric(horizontal: 100.0, vertical: 15.0),
            child: Text('Login'),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0)),
            onPressed: () => (_gologin(context)),
          ),
          SizedBox(height: 50.0, width: 500.0),
           (isLoggedIn)
              ? RaisedButton(
                  child: Text("Continuar con la sesion"),
                  onPressed: () => _gohome(context),
                )
              : RaisedButton(
                  child: Text("Login with Facebook"),
                  onPressed: () => initiateFacebookLogin(),
                ),
          RaisedButton(
            child: Text('Log out'),
            onPressed: ()=> _logout(),
          )
        ],
      ),
    );
  }

  _gologin(BuildContext context) {
    Navigator.pushReplacementNamed(context, 'login');
  }

  _gohome(BuildContext context) {
    Navigator.pushNamed(context, 'visit');
  }

  void initiateFacebookLogin() async {
    var facebookLogin = FacebookLogin();
    facebookLogin.loginBehavior = FacebookLoginBehavior.webViewOnly;
    var facebookLoginResult =
        await facebookLogin.logIn(['email']);
        print(facebookLoginResult.status);
    switch (facebookLoginResult.status) {
      case FacebookLoginStatus.error:
        print("Error");
        onLoginStatusChanged(false);
        break;
      case FacebookLoginStatus.cancelledByUser:
        print("CancelledByUser");
        onLoginStatusChanged(false);
        break;
      case FacebookLoginStatus.loggedIn:
        print("LoggedIn");
        onLoginStatusChanged(true);
        break;
    }
  }

  void onLoginStatusChanged(bool isLoggedIn) {
    setState(() {
      this.isLoggedIn = isLoggedIn;
    });
  }

  _logout() {
    var facebookLogin = FacebookLogin();
    facebookLogin.logOut();
    setState(() {
      isLoggedIn=false;
    });
  }
}
