import 'package:flutter/material.dart';
import 'package:turismo_app/src/home/models/producto_model.dart';
import 'package:turismo_app/src/home/providers/productos_provider.dart';


class SearchPage extends StatefulWidget {
  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  final productosProvider = new ProductosProvider();
  List<ProductoModel> _products = List<ProductoModel>();
  List<ProductoModel> _productsToDisplay = List<ProductoModel>();

  @override
  void initState() {
    super.initState();
    productosProvider.cargarProductos().then((value) {
      setState(() {
        _products.addAll(value);
        _productsToDisplay = _products;
      });
    });
    
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Container(
          child: _searchBar(),
        ),
      ),
      body: (_products.length != 0)
          ? GridView.builder(
              itemBuilder: (context, index) {
                return _listItem(index);
              },
              itemCount: _productsToDisplay.length,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2),
            )
          : Center(child: CircularProgressIndicator()));
  }

  _listItem(index) {
    return Card(
      child: Hero(
        tag: Text('${_productsToDisplay[index].titulo}'),
        child: Material(
          child: InkWell(
            onTap: ()=> Navigator.pushNamed(context, 'detail', arguments: _productsToDisplay[index]),
            child: GridTile(
              footer: Container(
                color: Colors.black38,
                child: ListTile(
                  leading: Text('${_productsToDisplay[index].titulo}',style: TextStyle(color: Colors.white,fontStyle: FontStyle.italic,fontWeight: FontWeight.bold),),
                ),
              ),
              child: (_productsToDisplay[index].fotoUrl==null) 
              ? Image(image: AssetImage('assets/no-image.png')) 
              :FadeInImage(
                image: NetworkImage(_productsToDisplay[index].fotoUrl),
                placeholder: AssetImage(
                  'assets/jar-loading.gif',
                ),
                fit: BoxFit.cover,
              ),
            ),
          ),
        ),
      ),
    );
  }

  _searchBar() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 5.0),
      child: TextField(
        decoration: InputDecoration(hintText: 'Buscar...', hintStyle: TextStyle(color: Colors.white) ),
        style: TextStyle(color: Colors.white),
        onChanged: (text) {
          text = text.toLowerCase();
          setState(() {
            _productsToDisplay = _products.where((note) {
              var noteTitle = note.titulo.toLowerCase();
              return noteTitle.contains(text);
            }).toList();
          });
        },
      ),
    );
  }
}
