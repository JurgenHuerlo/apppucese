import 'package:flutter/material.dart';
import 'package:turismo_app/src/home/models/producto_model.dart';
import 'package:turismo_app/src/home/providers/productos_provider.dart';

class Homepage extends StatelessWidget {
  final productosProvider = new ProductosProvider();
  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        title: Text(
          'Inicio',
          style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search),
            onPressed: ()=>Navigator.pushNamed(context, 'search'),
          ),
        ],
      ),
      body: _crearListado(),
      floatingActionButton: _crearBoton(context),
    );
  }

  Widget _crearListado(){

    return FutureBuilder(
      future: productosProvider.cargarProductos(),
      builder: (BuildContext context, AsyncSnapshot<List<ProductoModel>> snapshot) {
        if (snapshot.hasData) {
          return ListView.builder(
            itemCount: snapshot.data.length,
            itemBuilder: (context, i)=> _crearItem(context,snapshot.data[i]),

          );
        } else {
          return Center(child: CircularProgressIndicator());
        }
      },
    );

  }

  Widget _crearItem(BuildContext context, ProductoModel producto){
    return Dismissible(
      key: UniqueKey(),
      background: Container(
        color: Colors.redAccent,
        ),
      onDismissed: (direccion){
        productosProvider.borrarProducto(producto.id);
        mostrarSnackBar('Registro Eliminado');
      },
      child: Card(
        child: Column(
          children: <Widget>[
            (producto.fotoUrl==null)
            ? Image(image: AssetImage('assets/no-image.png'))
            : GestureDetector(
              child: FadeInImage(
                image: NetworkImage(producto.fotoUrl),
                placeholder: AssetImage('assets/jar-loading.gif'),
                height: 300.0,
                width: double.infinity,
                fit: BoxFit.contain,
                ),
              onTap: ()=> Navigator.pushNamed(context, 'detail', arguments: producto),
              onLongPress: ()=> Navigator.pushNamed(context, 'producto', arguments: producto),
            ),
              ListTile(
                title: Text('${producto.titulo}'),
                subtitle: Text(producto.direccion),
                onTap: ()=> Navigator.pushNamed(context, 'detail', arguments: producto)
              ),
          ],
        ),
      ),
    );
  }
     
  _crearBoton(BuildContext context) {
    return FloatingActionButton(
      child: Icon(Icons.add),
      backgroundColor: Colors.green,
      onPressed: ()=> Navigator.pushNamed(context, 'producto'),

    );
  }
  void mostrarSnackBar(String mensaje){
    final snackbar = SnackBar(
      content: Text(mensaje),
      duration: Duration(milliseconds: 1500),
    );

    scaffoldKey.currentState.showSnackBar(snackbar);
  }
}