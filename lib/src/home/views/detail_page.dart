import 'package:flutter/material.dart';
import 'package:turismo_app/src/home/models/producto_model.dart';

class DetailPage extends StatefulWidget {
  const DetailPage({Key key}) : super(key: key);

  @override
  _DetailPageState createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  
  ProductoModel producto = new ProductoModel();
  
  @override
  Widget build(BuildContext context) {
    final ProductoModel prodData = ModalRoute.of(context).settings.arguments;

    if (prodData != null) {
      producto = prodData;
    }
    
    return Scaffold(
        appBar: AppBar(
          title: Text(
            producto.titulo,
            style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
          ),
          centerTitle: true,
        ),
        body: Container(
            child: ListView(
                children: [
                  titleSection(),
                  dataSection(),
                  textSection(context)
                ]
            )
        )
    );
  }

  Widget titleSection() {
    return Stack(
      children: <Widget>[
        (producto.fotoUrl==null) 
        ? Image(image: AssetImage('assets/no-image.png'))
        :  FadeInImage(
          image: NetworkImage(producto.fotoUrl),
          placeholder: AssetImage('assets/jar-loading.gif'),
          height: 220.0,
          width: double.infinity,
          fit: BoxFit.cover,
        ),
      ],
    );
  }

  Widget dataSection() {
    return Container(
      padding: EdgeInsets.fromLTRB(10.0, 22.0, 32.0, 10.0),
      decoration: BoxDecoration(
        color: Color.fromRGBO(108, 180, 60, 0.8),
        
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Colors.black26,
            blurRadius: 3.0,
            offset: Offset(0.0, 5.0),
            spreadRadius: 3.0
          )
        ]
      ),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: EdgeInsets.only(bottom: 8.0),
                  child: Text(
                    'Dirección: \n ${producto.direccion}',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18.0,
                      color: Colors.white
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget textSection(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Column(
      children: <Widget>[
        SafeArea(
          child: Container(
            height: 1.0,
          ),
        ),
        Container(
          width: size.width * 0.90,
          margin: EdgeInsets.symmetric(vertical: 30.0),
          padding: EdgeInsets.symmetric(vertical: 30.0),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(15.0),
            boxShadow: <BoxShadow>[
              BoxShadow(
                color: Colors.black26,
                blurRadius: 3.0,
                offset: Offset(0.0, 5.0),
                spreadRadius: 3.0
              )
            ]
          ),
          child: texto(),
        ),
      ],
    );
  }
  Widget texto(){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      child: Text(
        'Descripción: \n${producto.descripcion}',
        softWrap: true,
        style: TextStyle(fontSize: 15.0),
      ),
    );
  }
}