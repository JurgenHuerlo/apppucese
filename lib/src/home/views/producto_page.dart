import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:turismo_app/src/home/models/producto_model.dart';
import 'package:turismo_app/src/home/providers/productos_provider.dart';


class ProductoPage extends StatefulWidget {

  @override
  _ProductoPageState createState() => _ProductoPageState();
}

class _ProductoPageState extends State<ProductoPage> {
  final formKey = GlobalKey<FormState>();
  final scaffoldKey = GlobalKey<ScaffoldState>();
  final productoProvider = new ProductosProvider();
  

  ProductoModel producto = new ProductoModel();
  bool _guardando = false;
  File foto;

  @override
  Widget build(BuildContext context) {
    final ProductoModel prodData = ModalRoute.of(context).settings.arguments;
    
    if (prodData != null) {
      producto = prodData;
    }

    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        title: Text('Producto'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.photo_size_select_actual),
            onPressed: _seleccionarFoto,
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(15.0),
          child: Form(
            key: formKey,
            child: Column(
              children: <Widget>[
                _mostrarFoto(),
                _crearNombre(),
                _crearDireccion(),
                _crearDescripcion(),
                SizedBox(height: 20.0,),
                _crearBoton(),
              ],
            ),
          ),
        ),
      ),
    );
  }

 Widget _crearNombre() {
   return TextFormField(
     initialValue: producto.titulo,
     textCapitalization: TextCapitalization.sentences,
     decoration: InputDecoration(
       labelText: 'Lugar'
     ),
     onSaved: (value)=>producto.titulo=value,
     validator: (value){
       if(value.length<3){
         return 'Ingrese el nombre del producto';
       }else{
         return null;
       }
     },
   );
 }

 Widget _crearDireccion() {
   return TextFormField(
     initialValue: producto.direccion,
     textCapitalization: TextCapitalization.sentences,
     decoration: InputDecoration(
       labelText: 'Direccion'
     ),
     onSaved: (value)=>producto.direccion=value,
     validator: (value){
       if(value.length<3){
         return 'Ingrese la direccion';
       }else{
         return null;
       }
     },
   );
 }

  Widget _crearDescripcion() {
   return TextFormField(
     initialValue: producto.descripcion,
     textCapitalization: TextCapitalization.sentences,
     decoration: InputDecoration(
       labelText: 'Descripcion'
     ),
     maxLines: null,
     keyboardType: TextInputType.multiline,
     onSaved: (value)=>producto.descripcion=value,
     validator: (value){
       if(value.length<3){
         return 'Ingrese la descripcion';
       }else{
         return null;
       }
     },
   );
 }

  Widget _crearBoton() {
    return RaisedButton.icon(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      color: Colors.green,
      textColor: Colors.white,
      label: Text('Guardar'),
      icon: Icon(Icons.save),
      onPressed: (_guardando) ? null :_submit,
    );
  }

  void _submit()async {
    //Cuando no es valido
    if(!formKey.currentState.validate()) return;

    formKey.currentState.save();
    
    setState(() {_guardando = true;});

    if (foto != null) {
      producto.fotoUrl = await productoProvider.subirImagen(foto);
    }

    //Cuando es valido
    if(producto.id == null){
      productoProvider.crearProducto(producto);
      mostrarSnackBar('Registro Creado');
      Navigator.pushReplacementNamed(context, 'home');
    }else{
      productoProvider.editarProducto(producto);
      mostrarSnackBar('Registro Actualizado');
      Navigator.pushReplacementNamed(context, 'home');
    }
  }

  void mostrarSnackBar(String mensaje){
    final snackbar = SnackBar(
      content: Text(mensaje),
      duration: Duration(milliseconds: 1500),
    );

    scaffoldKey.currentState.showSnackBar(snackbar);
  }

  _mostrarFoto(){
    if (producto.fotoUrl != null) {
      return FadeInImage(
        image: NetworkImage(producto.fotoUrl),
        placeholder: AssetImage('assets/jar-loading.gif'),
        height: 300.0,
        width: double.infinity,
        fit: BoxFit.contain,
      );
    }else{
      return (foto==null) ? Image(
        image: AssetImage('assets/no-image.png'),
        height: 300.0,
        fit: BoxFit.cover,
      ): Image.file(
        foto,
        height: 300.0,
        fit: BoxFit.cover,
      );
    }
  }

  void _seleccionarFoto() async {
    foto = await ImagePicker.pickImage(
      source: ImageSource.gallery
    );

    if (foto !=null) {
      producto.fotoUrl=null;
    }
    setState(() {});
  }
  
}