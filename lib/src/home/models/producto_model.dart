import 'dart:convert';

ProductoModel productoModelFromJson(String str) => ProductoModel.fromJson(json.decode(str));

String productoModelToJson(ProductoModel data) => json.encode(data.toJson());

class ProductoModel {
    String id;
    String titulo;
    String direccion;
    String descripcion;
    String fotoUrl;

    ProductoModel({
        this.id,
        this.titulo,
        this.direccion,
        this.descripcion,
        this.fotoUrl,
    });

    factory ProductoModel.fromJson(Map<String, dynamic> json) => ProductoModel(
        id: json["id"],
        titulo: json["titulo"],
        direccion: json["direccion"],
        descripcion: json["descripcion"],
        fotoUrl: json["fotoUrl"],
    );

    Map<String, dynamic> toJson() => {
        //"id": id,
        "titulo": titulo,
        "direccion": direccion,
        "descripcion": descripcion,
        "fotoUrl": fotoUrl,
    };
}