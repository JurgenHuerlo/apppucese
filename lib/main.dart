import 'package:flutter/material.dart';
import 'package:turismo_app/src/home/owner/views/home_page.dart';
import 'package:turismo_app/src/home/views/detail_page.dart';
import 'package:turismo_app/src/home/views/producto_page.dart';
import 'package:turismo_app/src/home/visit/views/visit_page.dart';
import 'package:turismo_app/src/login/provider/provider.dart';
import 'package:turismo_app/src/login/views/login_page.dart';
import 'package:turismo_app/src/search/views/search_page.dart';
import 'package:turismo_app/src/static/start_page.dart';

 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Provider(
        child: MaterialApp(
        title: 'Turismo',
        debugShowCheckedModeBanner: false,
        initialRoute: 'start',
        routes: {
          'home' : (BuildContext context)=>Homepage(),
          'login':  (BuildContext context)=>LoginPage(),
          'producto' : (BuildContext context)=>ProductoPage(),
          'detail' : (BuildContext context)=>DetailPage(),
          'start' : (BuildContext contex)=>Startpage(),
          'visit' : (BuildContext context)=>VisitPage(),
          'search': (BuildContext context)=>SearchPage(),
        },
        theme: ThemeData(
          primaryColor: Colors.green
        ),
      )
    );
  }
}